var SnailBait = function(){
    this.canvas = document.getElementById('snailbait-canvas'),
    this.context = this.canvas.getContext('2d'),
    this.fpsElement = document.getElementById('fps'),

    //Time
    this.timeSystem = new TimeSystem(),
    this.timeRate = 1,

    //Pixels and metres
    this.CANVAS_WIDTH_IN_METERS = 13,
    this.PIXELS_PER_METER = this.canvas.width/this.CANVAS_WIDTH_IN_METERS,

    //Gravity
    this.GRAVITY_FORCE = 9.81, //m/s/s

    //Constants
    this.LEFT = 1,
    this.RIGHT = 2,

    this.SHORTDELAY = 50,

    this.TRANSPARENT = 0,
    this.OPAQUE = 1.0,

    this.PLATFORM_HEIGHT = 8,
    this.PLATFORM_STOKE_WIDTH = 2,
    this.PLATFORM_STROKE_STYLE = 'rbg(0,0,0)', //black

    // Background width and height.........................................

   this.BACKGROUND_WIDTH = 1102,
   this.BACKGROUND_HEIGHT = 400,

    this.RUNNER_LEFT = 50,
    this.STARTING_RUNNER_TRACK = 1,

    //Loading screen
    this.loadingElement = document.getElementById('loading'),
    this.loadingTitleElement = document.getElementById('loading-title'),
    this.snailAnimatedGIFElement = document.getElementById('loading-animated-gif'),

    //Track baselines
    this.TRACK_1_BASELINE = 323,
    this.TRACK_2_BASELINE = 223,
    this.TRACK_3_BASELINE = 123,

    //Background scrolling
    this.BACKGROUND_VELOCITY = 25,
    this.RUN_ANIMATION_RATE = 15,
    this.STARTING_BACKGROUND_VELOCITY = 0,
    this.STARTING_BACKGROUND_OFFSET = 0,

    this.PLATFORM_VELOCITY_MULTIPIER = 4,
    this.STARTING_SPRITE_OFFSET = 0,

    //Pacing velocities
    this.BUTTON_PACE_VELOCITY = 80,
    this.SNAIL_PACE_VELOCITY = 30,

    //RUNNER EXPLOSION
    this.RUNNER_EXPLOSION_DURATION = 500,
    this.BAD_GUYS_EXPLOSION_DURATION = 3500,

    //Images
    this.spritesheet = new Image(),
    

    //Time
    this.lastAnimationFrameTime = 0,
    this.lastFpsUpdateTime = 0,
    this.fps = 60,

    //Toast..............................................................
    this.toastElement = document.getElementById('toast'),
    this.countdownInProgress = false,

    // Instructions......................................................

   this.instructionsElement = document.getElementById('instructions');

   // Copyright.........................................................

   this.copyrightElement = document.getElementById('copyright');

   // Score.............................................................

   this.scoreElement = document.getElementById('score'),

   // Sound and music...................................................

   this.soundAndMusicElement = document.getElementById('sound-and-music');

    //KEYS
    this.A_KEY = 65,
    this.LEFT_ARROW = 37,
    this.D_KEY = 68,
    this.RIGHT_ARROW = 39,
    this.W_KEY = 87,
    this.UP_ARROW = 38,
    this.P_KEY = 80,
    this.J_KEY = 74,

    //States
    this.paused = false,
    this.PAUSE_CHECK_INTERVAL = 200,
    this.pauseStartTime,
    this.windowHasFocus = true,
    this.playing = false,

    this.gameStarted = false,

    //Translation
    this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
    this.spriteOffset = this.STARTING_SPRITE_OFFSET,
    this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
    this.platformVelocity,

    //Runner track
    this.runnerTrack = this.STARTING_RUNNER_TRACK,

   // Sprite sheet cells................................................

   this.RUNNER_CELLS_WIDTH = 50; // pixels
   this.RUNNER_CELLS_HEIGHT = 54;

   this.BAT_CELLS_HEIGHT = 34; // Bat cell width varies; not constant 

   this.BEE_CELLS_HEIGHT = 50;
   this.BEE_CELLS_WIDTH  = 50;

   this.BUTTON_CELLS_HEIGHT  = 20;
   this.BUTTON_CELLS_WIDTH   = 31;

   this.COIN_CELLS_HEIGHT = 30;
   this.COIN_CELLS_WIDTH  = 30;
   
   this.EXPLOSION_CELLS_HEIGHT = 62;

   this.RUBY_CELLS_HEIGHT = 30;
   this.RUBY_CELLS_WIDTH = 35;

   this.SAPPHIRE_CELLS_HEIGHT = 30;
   this.SAPPHIRE_CELLS_WIDTH  = 35;

   this.SNAIL_BOMB_CELLS_HEIGHT = 20;
   this.SNAIL_BOMB_CELLS_WIDTH  = 20;

   this.SNAIL_CELLS_HEIGHT = 34;
   this.SNAIL_CELLS_WIDTH  = 64;

   this.batCells = [
      { left: 3,   top: 0, width: 36, height: this.BAT_CELLS_HEIGHT },
      { left: 41,  top: 0, width: 46, height: this.BAT_CELLS_HEIGHT },
      { left: 93,  top: 0, width: 36, height: this.BAT_CELLS_HEIGHT },
      { left: 132, top: 0, width: 46, height: this.BAT_CELLS_HEIGHT },
   ];

   this.batRedEyeCells = [
      { left: 185, top: 0, 
        width: 36, height: this.BAT_CELLS_HEIGHT },

      { left: 222, top: 0, 
        width: 46, height: this.BAT_CELLS_HEIGHT },

      { left: 273, top: 0, 
        width: 36, height: this.BAT_CELLS_HEIGHT },

      { left: 313, top: 0, 
        width: 46, height: this.BAT_CELLS_HEIGHT },
   ];
   
   this.beeCells = [
      { left: 5,   top: 234, width: this.BEE_CELLS_WIDTH,
                            height: this.BEE_CELLS_HEIGHT },

      { left: 75,  top: 234, width: this.BEE_CELLS_WIDTH, 
                            height: this.BEE_CELLS_HEIGHT },

      { left: 145, top: 234, width: this.BEE_CELLS_WIDTH, 
                            height: this.BEE_CELLS_HEIGHT }
   ];
   
   this.blueCoinCells = [
      { left: 5, top: 540, width: this.COIN_CELLS_WIDTH, 
                           height: this.COIN_CELLS_HEIGHT },

      { left: 5 + this.COIN_CELLS_WIDTH, top: 540,
        width: this.COIN_CELLS_WIDTH, 
        height: this.COIN_CELLS_HEIGHT }
   ];

   this.explosionCells = [
      { left: 3,   top: 48, 
        width: 52, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 63,  top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 146, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 233, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 308, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 392, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 473, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT }
   ];

   // Sprite sheet cells................................................

   this.blueButtonCells = [
      { left: 10,   top: 192, width: this.BUTTON_CELLS_WIDTH,
                            height: this.BUTTON_CELLS_HEIGHT },

      { left: 53,  top: 192, width: this.BUTTON_CELLS_WIDTH, 
                            height: this.BUTTON_CELLS_HEIGHT }
   ];

   this.goldCoinCells = [
      { left: 65, top: 540, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 96, top: 540, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 128, top: 540, width: this.COIN_CELLS_WIDTH, 
                             height: this.COIN_CELLS_HEIGHT },
   ];

   this.goldButtonCells = [
      { left: 90,   top: 190, width: this.BUTTON_CELLS_WIDTH,
                              height: this.BUTTON_CELLS_HEIGHT },

      { left: 132,  top: 190, width: this.BUTTON_CELLS_WIDTH,
                              height: this.BUTTON_CELLS_HEIGHT }
   ];

   this.rubyCells = [
      { left: 3,   top: 138, width: this.RUBY_CELLS_WIDTH,
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 39,  top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 76,  top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 112, top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 148, top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT }
   ];

   this.runnerCellsRight = [
      { left: 414, top: 385, 
        width: 47, height: this.RUNNER_CELLS_HEIGHT },

      { left: 362, top: 385, 
         width: 44, height: this.RUNNER_CELLS_HEIGHT },

      { left: 314, top: 385, 
         width: 39, height: this.RUNNER_CELLS_HEIGHT },

      { left: 265, top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 205, top: 385, 
         width: 49, height: this.RUNNER_CELLS_HEIGHT },

      { left: 150, top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 96,  top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 45,  top: 385, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },

      { left: 0,   top: 385, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT }
   ],

   this.runnerCellsLeft = [
      { left: 0,   top: 305, 
         width: 47, height: this.RUNNER_CELLS_HEIGHT },

      { left: 55,  top: 305, 
         width: 44, height: this.RUNNER_CELLS_HEIGHT },

      { left: 107, top: 305, 
         width: 39, height: this.RUNNER_CELLS_HEIGHT },

      { left: 152, top: 305, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 208, top: 305, 
         width: 49, height: this.RUNNER_CELLS_HEIGHT },

      { left: 265, top: 305, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 320, top: 305, 
         width: 42, height: this.RUNNER_CELLS_HEIGHT },

      { left: 380, top: 305, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },

      { left: 425, top: 305, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },
   ],

   this.sapphireCells = [
      { left: 185,   top: 138, width: this.SAPPHIRE_CELLS_WIDTH,
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 220,  top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 258,  top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 294, top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 331, top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT }
   ];

   this.snailBombCells = [
      { left: 40, top: 512, width: 30, height: 20 },
      { left: 2, top: 512, width: 30, height: 20 }
   ];

   this.snailCells = [
      { left: 142, top: 466, width: this.SNAIL_CELLS_WIDTH,
                             height: this.SNAIL_CELLS_HEIGHT },

      { left: 75,  top: 466, width: this.SNAIL_CELLS_WIDTH, 
                             height: this.SNAIL_CELLS_HEIGHT },

      { left: 2,   top: 466, width: this.SNAIL_CELLS_WIDTH, 
                             height: this.SNAIL_CELLS_HEIGHT },
   ]; 

   // Sprite data.......................................................

   this.batData = [
      { left: 95,  
         top: this.TRACK_2_BASELINE - 1.5*this.BAT_CELLS_HEIGHT },

      { left: 624,  
         top: this.TRACK_3_BASELINE },

      { left: 804,  
         top: this.TRACK_3_BASELINE - 3*this.BAT_CELLS_HEIGHT },

      { left: 1180, 
         top: this.TRACK_2_BASELINE - 2*this.BAT_CELLS_HEIGHT },

      { left: 1720, 
         top: this.TRACK_2_BASELINE - 2*this.BAT_CELLS_HEIGHT },

      { left: 1960, 
         top: this.TRACK_3_BASELINE - this.BAT_CELLS_HEIGHT }, 

      { left: 2200, 
         top: this.TRACK_3_BASELINE - this.BAT_CELLS_HEIGHT },

      { left: 2380, 
         top: this.TRACK_3_BASELINE - 2*this.BAT_CELLS_HEIGHT }
   ];
   
   this.beeData = [
      { left: 225,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT*1.25 },
      { left: 355,  
         top: this.TRACK_2_BASELINE - this.BEE_CELLS_HEIGHT*1.25 },
      { left: 520,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT },
      { left: 780,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT*1.25 },

      { left: 924,  
         top: this.TRACK_2_BASELINE + this.BEE_CELLS_HEIGHT*1.25 },

      { left: 1500, top: 225 },
      { left: 1600, top: 115 },
      { left: 2225, top: 125 },
      { left: 2195, top: 275 },
      { left: 2450, top: 275 }
   ];
   
   this.buttonData = [
      { platformIndex: 7 },
      { platformIndex: 12 },
   ];

   this.coinData = [
      { left: 270,  
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 489,  
         top: this.TRACK_3_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 620,  
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 833,  
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1050, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1450, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1670, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1870, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1930, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2200, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2320, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2360, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 
   ];  


    //Platforms
    this.platformData = [
        // Screen 1.......................................................
        {
         left:      10,
         width:     210,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200, 200, 60)',
         opacity:   1.0,
         track:     1,
         pulsate:   false,
      },

      {  left:      240,
         width:     110,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(110,150,255)',
         opacity:   1.0,
         track:     2,
         pulsate:   false,
      },

      {  left:      400,
         width:     125,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(250,0,0)',
         opacity:   1.0,
         track:     3,
         pulsate:   false
      },

      {  left:      603,
         width:     250,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(255,255,0)',
         opacity:   0.8,
         track:     1,
         pulsate:   false,
      },

      // Screen 2.......................................................
               
      {  left:      810,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200,200,0)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1005,
         width:     150,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1200,
         width:     105,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   1.0,
         track:     3,
         pulsate:   false
      },

      {  left:      1400,
         width:     180,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   1.0,
         track:     1,
         pulsate:   false,
      },

      // Screen 3.......................................................
               
      {  left:      1625,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'cornflowerblue',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1800,
         width:     250,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'gold',
         opacity:   1.0,
         track:     1,
         pulsate:   false
      },

      {  left:      2000,
         width:     200,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200,200,80)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      2100,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   1.0,
         track:     3,
         pulsate:   false
      },


      // Screen 4.......................................................

      {  left:      2269,
         width:     200,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'gold',
         opacity:   1.0,
         track:     1,
         pulsate:   true
      },

      {  left:      2500,
         width:     200,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: '#2b950a',
         opacity:   1.0,
         track:     2,
         pulsate:   true
      }
   ];

   this.sapphireData = [
      { left: 155,  
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 880,  
         top: this.TRACK_2_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 1100, 
         top: this.TRACK_2_BASELINE - this.SAPPHIRE_CELLS_HEIGHT }, 

      { left: 1475, 
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 2400, 
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT }
   ];

   this.rubyData = [
      { left: 690,  
         top: this.TRACK_1_BASELINE - this.RUBY_CELLS_HEIGHT },

      { left: 1700, 
         top: this.TRACK_2_BASELINE - this.RUBY_CELLS_HEIGHT },

      { left: 2056, 
         top: this.TRACK_2_BASELINE - this.RUBY_CELLS_HEIGHT },
   ];

   this.smokingHoleData = [
      { left: 248,  top: this.TRACK_2_BASELINE - 22 },
      { left: 688,  top: this.TRACK_3_BASELINE + 5 },
      { left: 1352,  top: this.TRACK_2_BASELINE - 18 },
   ];
   
   this.snailData = [
      { platformIndex: 13 },
   ];

   // Sprites...........................................................
  
   this.bats         = [];
   this.bees         = []; 
   this.buttons      = [];
   this.coins        = [];
   this.platforms    = [];
   this.rubies       = [];
   this.sapphires    = [];
   this.snails       = [];

   this.sprites = []; // For convenience, contains all of the sprites  
                      // from the preceding arrays

   this.platformArtist = {
      draw: function (sprite, context) {
         var PLATFORM_STROKE_WIDTH = 1.0,
             PLATFORM_STROKE_STYLE = 'black',
             top;
         
         top = snailBait.calculatePlatformTop(sprite.track);

         context.lineWidth = PLATFORM_STROKE_WIDTH;
         context.strokeStyle = PLATFORM_STROKE_STYLE;
         context.fillStyle = sprite.fillStyle;

         context.strokeRect(sprite.left, top, 
                            sprite.width, sprite.height);

         context.fillRect  (sprite.left, top, 
                            sprite.width, sprite.height);
      }
   };
   //Sprite Behaviours----------------------------
   //Runner run behaviour
   this.runBehavior = {
      lastAdvanceTime: 0,

      execute: function(sprite, now, fps, 
         context, lastAnimationFrameTime){
            if(sprite.runAnimationRate === 0){
               return;
            }
            if(this.lastAdvanceTime === 0){
               this.lastAdvanceTime = now;
            }
            else if(now - this.lastAdvanceTime > 
               1000/sprite.runAnimationRate){
                  sprite.artist.advance();
                  this.lastAdvanceTime = now;
               }
         }
   };

   //Pacing on a platform behaviour
   this.paceBehavior = {
      setDirection: function(sprite){
         var sRight = sprite.left + sprite.width,
         pRight = sprite.platform.left + sprite.platform.width;

         if(sprite.direction === undefined){
            sprite.direction = snailBait.RIGHT;
         }

         if(sRight > pRight && 
            sprite.direction === snailBait.RIGHT){
               sprite.direction = snailBait.LEFT;
            }
         else if(sprite.left < sprite.platform.left && 
               sprite.direction === snailBait.LEFT){
                  sprite.direction = snailBait.RIGHT;
               }
      },

      setPosition: function(sprite, now, lastAnimationFrameTime){
         var pixelsToMove = sprite.velocityX * 
         (now - lastAnimationFrameTime)/1000;
         if(sprite.direction === snailBait.RIGHT){
            sprite.left += pixelsToMove;
         }
         else{
            sprite.left -= pixelsToMove;
         }
      },

      execute: function(sprite, now, fps, context, 
         lastAnimationFrameTime){
            this.setDirection(sprite);
            this.setPosition(sprite, now, lastAnimationFrameTime);
         }
   };

   // Snail shoot behavior..............................................

   this.snailShootBehavior = { // sprite is the snail
      execute: function (sprite, now, fps, context, 
                         lastAnimationFrameTime) {
         var bomb = sprite.bomb,
                    MOUTH_OPEN_CELL = 2;

         if ( ! snailBait.isSpriteInView(sprite)) {
            return;
         }

         if ( ! bomb.visible && 
              sprite.artist.cellIndex === MOUTH_OPEN_CELL) {
            bomb.left = sprite.left;
            bomb.visible = true;
         }
      }
   };

   // Move the snail bomb...............................................

   this.snailBombMoveBehavior = {
      execute: function (sprite, now, fps, context, 
                         lastAnimationFrameTime) {
         var SNAIL_BOMB_VELOCITY = 550;
     
         if ( sprite.left + sprite.width > sprite.hOffset &&
              sprite.left + sprite.width < sprite.hOffset + sprite.width) {
            sprite.visible = false;
         }
         else {
            sprite.left -= SNAIL_BOMB_VELOCITY * 
                           ((now - lastAnimationFrameTime) / 1000);

         }
      }
   };

   //Runner explosions
   this.runnerExplodeBehavior = new CellSwitchBehavior(
      this.explosionCells, this.RUNNER_EXPLOSION_DURATION,
       function(sprite, now, fps){ //Trigger
         return sprite.exploding;
      },

      function(sprite){ //Callback
         sprite.exploding = false;
      }
   );

   this.jumpBehavior = {
      pause: function (sprite) {
         if (sprite.ascendTimer.isRunning()) {
            sprite.ascendTimer.pause();
         }
         else if (sprite.descendTimer.isRunning()) {
            sprite.descendTimer.pause();
         }
      },

      unpause: function (sprite) {
         if (sprite.ascendTimer.isRunning()) {
            sprite.ascendTimer.unpause();
         }         
         else if (sprite.descendTimer.isRunning()) {
            sprite.descendTimer.unpause();
         }
      },

      isAscending: function (sprite) {
         return sprite.ascendTimer.isRunning();
      },

      ascend: function (sprite) {
         var elapsed = sprite.ascendTimer.getElapsedTime(),
             deltaY  = elapsed / (sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;

         sprite.top = sprite.verticalLaunchPosition - deltaY; // Moving up
      },

      isDoneAscending: function (sprite) {
         return sprite.ascendTimer.getElapsedTime() > sprite.JUMP_DURATION/2;
      },

      finishAscent: function (sprite) {
         sprite.jumpApex = sprite.top;
         sprite.ascendTimer.stop();
         sprite.descendTimer.start();
      },

      isDescending: function (sprite) {
         return sprite.descendTimer.isRunning();
      },

      descend: function (sprite, verticalVelocity, fps) {
         var elapsed = sprite.descendTimer.getElapsedTime(),
             deltaY  = elapsed / (sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;

         sprite.top = sprite.jumpApex + deltaY; // Moving down
      },

      isDoneDescending: function (sprite) {
         return sprite.descendTimer.getElapsedTime() > sprite.JUMP_DURATION/2;
      },

      finishDescent: function (sprite, now) {
         sprite.stopJumping();

         if(snailBait.platformUnderneath(sprite)){
            sprite.top = sprite.verticalLaunchPosition;
         }
         else{
            sprite.fall(snailBait.GRAVITY_FORCE * 
               (sprite.descendTimer.getElapsedTime(now)/1000) *
               snailBait.PIXELS_PER_METER );
         }
         sprite.runAnimationRate = snailBait.RUN_ANIMATION_RATE;
      },

      execute: function (sprite, now, fps, context, 
                         lastAnimationFrameTime) {
         if ( ! sprite.jumping) {
            return;
         }

         if (this.isAscending(sprite)) {
            if ( ! this.isDoneAscending(sprite)) this.ascend(sprite);
            else                               this.finishAscent(sprite);
         }
         else if (this.isDescending(sprite)) {
            if ( ! this.isDoneDescending(sprite)) this.descend(sprite);
            else                                  this.finishDescent(sprite);
         }
      }
   };

   //Runner's fall behavior
   this.fallBehavior = {
      pause: function(sprite, now){
         sprite.fallTimer.pause(now);
      },

      unpause: function(sprite, now){
         sprite.fallTimer.unpause(now);
      },

      isOutOfPlay: function(sprite){
         return sprite.top > snailBait.canvas.height;
      },

      setSpriteVelocity: function(sprite, now){
         sprite.velocityY = sprite.initialVelocityY +
         snailBait.GRAVITY_FORCE * 
         (sprite.fallTimer.getElapsedTime(now)/1000) * 
         snailBait.PIXELS_PER_METER;
      },

      calculateVerticalDrop: function(sprite, now, lastAnimationFrameTime){
         return sprite.velocityY * (now - lastAnimationFrameTime)/1000; 
      },

      willFallBelowCurrentTrack: function(sprite, dropDistance){
         return sprite.top + sprite.height + dropDistance > 
         snailBait.calculatePlatformTop(sprite.track);
      },

      fallOnPlatform: function(sprite){
         sprite.stopFalling();
         snailBait.putSpriteOnTrack(sprite, sprite.track);
      },

      moveDown: function(sprite, now, lastAnimationFrameTime){
         var dropDistance;

         this.setSpriteVelocity(sprite, now);

         dropDistance = this.calculateVerticalDrop(sprite, now, lastAnimationFrameTime);
         if(!this.willFallBelowCurrentTrack(sprite, dropDistance)){
            sprite.top += dropDistance;
         }
         else{
            if(snailBait.platformUnderneath(sprite)){
               this.fallOnPlatform(sprite);
               sprite.stopFalling();
            }
            else{
               sprite.track--;
               sprite.top += dropDistance;
            }
         }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime){
         if(!sprite.falling){
            if(!sprite.jumping && !snailBait.platformUnderneath(sprite)){
               sprite.fall();
            }
         }
         else{
            if(this.isOutOfPlay(sprite)){
               sprite.stopFalling();
               snailBait.loseLife();
            }
            else{
               this.moveDown(sprite, now, lastAnimationFrameTime);
            }
         }
      }
   };

   //Detonate buttons
   this.blueButtonDetonateBehavior = {
      execute: function(sprite, now, fps, lastAnimationFrameTime){
         var BUTTON_REBOUND_DELAY = 1000,
             SECOND_BEE_EXPLOSION_DELAY = 400; // milliseconds

         if ( ! sprite.detonating) { // trigger
            return;
         }

         sprite.artist.cellIndex = 1; // flatten the button

         snailBait.explode(snailBait.bees[5]);

         setTimeout( function () {
            snailBait.explode(snailBait.bees[6]);
         }, SECOND_BEE_EXPLOSION_DELAY);

         sprite.detonating = false; // reset trigger

         setTimeout( function () {
            sprite.artist.cellIndex = 0; // rebound
         }, BUTTON_REBOUND_DELAY);
      }
   };

   this.collideBehavior = {
      isCandidateForCollision: function(sprite, otherSprite){
         var s, o;

         if (! sprite.calculateCollisionRectangle ||
            ! otherSprite.calculateCollisionRectangle) {
           return false;
        }

         s = sprite.calculateCollisionRectangle();
         o = otherSprite.calculateCollisionRectangle();

         return o.left < s.right && sprite !== 
         otherSprite && sprite.visible && 
         otherSprite.visible && !sprite.exploding 
         && !otherSprite.exploding;
      },

      didCollide: function(sprite, otherSprite, context){
         var o = otherSprite.calculateCollisionRectangle(),
             r = sprite.calculateCollisionRectangle();

         if (otherSprite.type === 'snail bomb') {
            return this.didRunnerCollideWithSnailBomb(r, o, context);
         }
         else {
            return this.didRunnerCollideWithOtherSprite(r, o, context);
         }
      },
      
      didRunnerCollideWithSnailBomb: function (r, o, context) {
         // Determine if the center of the snail bomb lies
         // within the runner's bounding box.

         context.beginPath();
         context.rect(r.left + snailBait.spriteOffset, 
                      r.top, r.right - r.left, r.bottom - r.top);

         return context.isPointInPath(o.centerX, o.centerY);
      },

      didRunnerCollideWithOtherSprite: function (r, o, context) {
         // Determine if either of the runner's four corners or its
         // center lie within the other sprite's bounding box.

         context.beginPath();
         context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);

         return context.isPointInPath(r.left,  r.top)       ||
                context.isPointInPath(r.right, r.top)       ||

                context.isPointInPath(r.centerX, r.centerY) ||

                context.isPointInPath(r.left,  r.bottom)    ||
                context.isPointInPath(r.right, r.bottom);
      },

      processPlatformCollisionDuringJump: function(sprite, platform){
            var isDescending = sprite.descendTimer.isRunning();
            sprite.stopJumping();
            sprite.runAnimationRate = snailBait.RUN_ANIMATION_RATE;
            if (isDescending) {
               snailBait.putSpriteOnTrack(sprite, platform.track);
            }
            else {
               sprite.fall();
            }
         },

      processBadGuyCollision: function(sprite){
         snailBait.runner.stopFalling();
         snailBait.runner.stopJumping();
         snailBait.explode(sprite);
         snailBait.shake();
         snailBait.loseLife();
      },

      processAssetCollision: function(otherSprite){
         otherSprite.visible = false;
      },

      processCollision: function(sprite, otherSprite){
         if(sprite.jumping && 'platform' === 
         otherSprite.type){
            this.processPlatformCollisionDuringJump(sprite,
               otherSprite);
         }
         else if('bat' === otherSprite.type || 'bee' === 
         otherSprite.type || 'snailBomb' === otherSprite.type ){
            this.processBadGuyCollision(sprite);
         }
         else if('coin' === otherSprite.type ||
         'sapphire' === otherSprite.type || 'ruby' === otherSprite.type){
            this.processAssetCollision(otherSprite);
         }
         else if('button' === otherSprite.type){
            if(sprite.jumping && sprite.descendTimer.isRunning()
            || sprite.falling){
               otherSprite.detonating = true;
            }
         }
      },

      execute: function(sprite, now, fps, context, 
         lastAnimationFrameTime){
            var otherSprite;

            if ( ! snailBait.playing) {
               return;
            }

            for(var i=0; i < snailBait.sprites.length; ++i){
               otherSprite = snailBait.sprites[i];
               if(this.isCandidateForCollision(sprite, otherSprite)){
                  if(this.didCollide(sprite, otherSprite, context)){
                     this.processCollision(sprite, otherSprite);
                  }
               }
            }
         }
   };


};

SnailBait.prototype = {

   createSprites: function () {
      this.createPlatformSprites(); 

      this.createBatSprites();
      this.createBeeSprites();
      this.createButtonSprites();
      this.createCoinSprites();
      this.createRunnerSprite(); 
      this.createRubySprites();
      this.createSapphireSprites();
      this.createSnailSprites();

      this.initializeSprites();

      // All sprites are also stored in a single array

      this.addSpritesToSpriteArray();
   },

   addSpritesToSpriteArray: function () {
      for (var i=0; i < this.platforms.length; ++i) {
         this.sprites.push(this.platforms[i]);
      }

      for (var i=0; i < this.bats.length; ++i) {
         this.sprites.push(this.bats[i]);
      }

      for (var i=0; i < this.bees.length; ++i) {
         this.sprites.push(this.bees[i]);
      }

      for (var i=0; i < this.buttons.length; ++i) {
         this.sprites.push(this.buttons[i]);
      }

      for (var i=0; i < this.coins.length; ++i) {
         this.sprites.push(this.coins[i]);
      }

      for (var i=0; i < this.rubies.length; ++i) {
         this.sprites.push(this.rubies[i]);
      }

      for (var i=0; i < this.sapphires.length; ++i) {
         this.sprites.push(this.sapphires[i]);
      }

      for (var i=0; i < this.snails.length; ++i) {
         this.sprites.push(this.snails[i]);
      }

      this.sprites.push(this.runner);
   },

   positionSprites: function (sprites, spriteData) {
      var sprite;

      for (var i = 0; i < sprites.length; ++i) {
         sprite = sprites[i];

         if (spriteData[i].platformIndex) { 
            this.putSpriteOnPlatform(sprite,
               this.platforms[spriteData[i].platformIndex]);
         }
         else {
            sprite.top  = spriteData[i].top;
            sprite.left = spriteData[i].left;
         }
      }
   },

   equipRunnerForJumping: function () {
      var INITIAL_TRACK = 1,
          RUNNER_JUMP_HEIGHT = 120,
          RUNNER_JUMP_DURATION = 1000;

      this.runner.JUMP_HEIGHT   = RUNNER_JUMP_HEIGHT;
      this.runner.JUMP_DURATION = RUNNER_JUMP_DURATION;

      this.runner.jumping = false;
      this.runner.track   = INITIAL_TRACK;

      this.runner.ascendTimer  = new AnimationTimer(this.runner.JUMP_DURATION/2,
         AnimationTimer.makeEaseOutEasingFunction(1.1));
      this.runner.descendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2,
         AnimationTimer.makeEaseInEasingFunction(1.1));

      this.runner.jump = function () {
         if (this.jumping) // 'this' is the runner
            return;

         this.jumping = true;
         this.runAnimationRate = 0; // Freeze the runner while jumping
         this.verticalLaunchPosition = this.top;
         this.ascendTimer.start();
      };

      this.runner.stopJumping = function () {
         this.descendTimer.stop();
         this.runAnimationRate = snailBait.RUN_ANIMATION_RATE;
         this.jumping = false;
      };
   },

   equipRunnerForFalling: function(){
      this.runner.fallTimer = new AnimationTimer();
      this.runner.falling = false;

      this.runner.fall = function(initialVelocity){
         this.falling = true;
         this.velocityY = initialVelocity || 0;
         this.initialVelocityY = initialVelocity || 0;
         this.fallTimer.start(
            snailBait.timeSystem.calculateGameTime());
      };

      this.runner.stopFalling = function(){
         this.falling = false;
         this.velocityY = 0;
         this.fallTimer.stop(
            snailBait.timeSystem.calculateGameTime());
      };
   },

   equipRunner: function () {
      this.equipRunnerForJumping();
      this.equipRunnerForFalling();
      this.runner.direction = snailBait.LEFT;
   },
   
   initializeSprites: function() {  
      this.positionSprites(this.bats,      this.batData);
      this.positionSprites(this.bees,      this.beeData);
      this.positionSprites(this.buttons,   this.buttonData);
      this.positionSprites(this.coins,     this.coinData);
      this.positionSprites(this.rubies,    this.rubyData);
      this.positionSprites(this.sapphires, this.sapphireData);
      this.positionSprites(this.snails,    this.snailData);

      this.armSnails();
      this.equipRunner();
   },

   createBatSprites: function () {
      var bat,
          BAT_FLAP_DURATION = 200,
          BAT_FLAP_INTERVAL = 50;

      for (var i = 0; i < this.batData.length; ++i) {
         bat = new Sprite('bat',
                          new SpriteSheetArtist(this.spritesheet, 
                                                this.batCells), 
                                                [ new CycleBehavior(BAT_FLAP_DURATION,
                                                   BAT_FLAP_INTERVAL) ]);

         // bat cell width varies; batCells[1] is widest

         bat.width = this.batCells[1].width; 
         bat.height = this.BAT_CELLS_HEIGHT;

         this.bats.push(bat);
      }
   },

   createBeeSprites: function () {
      var bee,
          beeArtist,
          BEE_FLAP_DURATION = 100,
          BEE_FLAP_INTERVAL = 30;

      for (var i = 0; i < this.beeData.length; ++i) {
         bee = new Sprite('bee',
                  new SpriteSheetArtist(this.spritesheet,                      
                     this.beeCells), 
                  [ new CycleBehavior(BEE_FLAP_DURATION,
                     BEE_FLAP_INTERVAL),
      new CellSwitchBehavior(this.explosionCells, 
         this.BAD_GUYS_EXPLOSION_DURATION,
         function(sprite, now, fps){
            return sprite.exploding;
         },
         function(sprite){
            sprite.exploding = false;
         }
         )]);

         bee.width = this.BEE_CELLS_WIDTH;
         bee.height = this.BEE_CELLS_HEIGHT;

         this.bees.push(bee);
      }
   },

   createButtonSprites: function () {
      var button;

      for (var i = 0; i < this.buttonData.length; ++i) {
         if (i !== this.buttonData.length - 1) {
            button = new Sprite('button',
                        new SpriteSheetArtist(this.spritesheet,
                                              this.blueButtonCells), 
                                              [this.paceBehavior, this.blueButtonDetonateBehavior]);
         }
         else {
            button = new Sprite('button',
                        new SpriteSheetArtist(this.spritesheet,
                                              this.goldButtonCells), [this.paceBehavior]);
         }
        
         button.width = this.BUTTON_CELLS_WIDTH;
         button.height = this.BUTTON_CELLS_HEIGHT;
         button.velocityX = this.BUTTON_PACE_VELOCITY;

         this.buttons.push(button);
      }
   },
   
   createCoinSprites: function () {
      var BLUE_THROB_DURATION = 100,
          GOLD_THROB_DURATION = 500,
          BOUNCE_DURATION_BASE = 800, // milliseconds
          BOUNCE_HEIGHT_BASE = 50,   // pixels
          coin;
   
      for (var i = 0; i < this.coinData.length; ++i) {
         if (i % 2 === 0) {
            coin = new Sprite('coin',
               new SpriteSheetArtist(this.spritesheet,
                                     this.goldCoinCells),
          
               [ new BounceBehavior(BOUNCE_DURATION_BASE +
                                    BOUNCE_DURATION_BASE * Math.random(),

                                    BOUNCE_HEIGHT_BASE +
                                    BOUNCE_HEIGHT_BASE * Math.random()),

                 new CycleBehavior(GOLD_THROB_DURATION) 
               ]
            );
         }
         else {
            coin = new Sprite('coin',
               new SpriteSheetArtist(this.spritesheet,
                                     this.blueCoinCells),

               [ new BounceBehavior(BOUNCE_DURATION_BASE +
                                    BOUNCE_DURATION_BASE * Math.random(),

                                    BOUNCE_HEIGHT_BASE +
                                    BOUNCE_HEIGHT_BASE * Math.random()),


                new CycleBehavior(BLUE_THROB_DURATION) ]);
         }
         
         coin.width = this.COIN_CELLS_WIDTH;
         coin.height = this.COIN_CELLS_HEIGHT;
         coin.value = 50;

         this.coins.push(coin);
      }
   },

   createPlatformSprites: function () {
      var sprite, pd,  // Sprite, Platform data
          PULSE_DURATION = 800,
          PULSE_OPACITY_THRESHOLD = 0.1;

      for (var i=0; i < this.platformData.length; ++i) {
         pd = this.platformData[i];

         sprite = new Sprite('platform', this.platformArtist);

         sprite.left = pd.left;
         sprite.width = pd.width;
         sprite.height = pd.height;
         sprite.fillStyle = pd.fillStyle;
         sprite.opacity = pd.opacity;
         sprite.track = pd.track;
         sprite.button = pd.button;
         sprite.pulsate = pd.pulsate;

         sprite.top = this.calculatePlatformTop(pd.track);

         if (sprite.pulsate) {
            sprite.behaviors = 
               [ new PulseBehavior(PULSE_DURATION, 
                                   PULSE_OPACITY_THRESHOLD) ];
         }

         this.platforms.push(sprite);
      }
   },
   
   createRubySprites: function () {
      var RUBY_SPARKLE_DURATION = 100,
          RUBY_SPARKLE_INTERVAL = 500,
          ruby,
          rubyArtist = new SpriteSheetArtist(this.spritesheet,
                                             this.rubyCells);
   
      for (var i = 0; i < this.rubyData.length; ++i) {
         ruby = new Sprite('ruby', rubyArtist, 
         [new CycleBehavior(RUBY_SPARKLE_DURATION, RUBY_SPARKLE_INTERVAL)]);

         ruby.width = this.RUBY_CELLS_WIDTH;
         ruby.height = this.RUBY_CELLS_HEIGHT;
         ruby.value = 200;
         
         this.rubies.push(ruby);
      }
   },

   createRunnerSprite: function () {
      var RUNNER_LEFT = 50,
          RUNNER_HEIGHT = 55,
          STARTING_RUNNER_TRACK = 1,
          STARTING_RUN_ANIMATION_RATE = this.RUN_ANIMATION_RATE;

       this.runner = new Sprite('runner',
                        new SpriteSheetArtist(this.spritesheet,
                                              this.runnerCellsRight), 
                                              [this.runBehavior, this.jumpBehavior, 
                                                this.collideBehavior, this.fallBehavior,
                                             this.runnerExplodeBehavior]); 

       this.runner.height = this.RUNNER_CELLS_HEIGHT;
       this.runner.width = this.RUNNER_CELLS_WIDTH;
       this.runner.runAnimationRate = STARTING_RUN_ANIMATION_RATE;                                             
       this.runner.track = STARTING_RUNNER_TRACK;
       this.runner.left = RUNNER_LEFT;
       this.runner.top = this.calculatePlatformTop(this.runner.track) -
                            RUNNER_HEIGHT;

       this.sprites.push(this.runner);
   },

   createSapphireSprites: function () {
      var SAPPHIRE_SPARKLE_DURATION = 100,
          SAPPHIRE_SPARKLE_INTERVAL = 300,
          sapphire,
          sapphireArtist = new SpriteSheetArtist(this.spritesheet,
                                                 this.sapphireCells);
   
      for (var i = 0; i < this.sapphireData.length; ++i) {
         sapphire = new Sprite('sapphire', sapphireArtist, 
         [new CycleBehavior(SAPPHIRE_SPARKLE_DURATION, SAPPHIRE_SPARKLE_INTERVAL)]);

         sapphire.width = this.SAPPHIRE_CELLS_WIDTH;
         sapphire.height = this.SAPPHIRE_CELLS_HEIGHT;
         sapphire.value = 100;

         this.sapphires.push(sapphire);
      }
   },

   createSnailSprites: function () {
      var snail,
          snailArtist = new SpriteSheetArtist(
             this.spritesheet,this.snailCells);
   
      for (var i = 0; i < this.snailData.length; ++i) {
         snail = new Sprite('snail', snailArtist, [this.paceBehavior, 
            this.snailShootBehavior, new CycleBehavior(300, 1500)]);

         snail.width  = this.SNAIL_CELLS_WIDTH;
         snail.height = this.SNAIL_CELLS_HEIGHT;
         snail.velocityX = snailBait.SNAIL_PACE_VELOCITY;

         this.snails.push(snail);
      }
   },

   armSnails: function () {
      var snail,
          snailBombArtist = new SpriteSheetArtist(this.spritesheet,
                                                  this.snailBombCells);

      for (var i=0; i < this.snails.length; ++i) {
         snail = this.snails[i];
         snail.bomb = new Sprite('snail bomb',
                                  snailBombArtist,
                                  [ this.snailBombMoveBehavior ]);

         snail.bomb.width  = snailBait.SNAIL_BOMB_CELLS_WIDTH;
         snail.bomb.height = snailBait.SNAIL_BOMB_CELLS_HEIGHT;

         snail.bomb.top = snail.top + snail.bomb.height/2;
         snail.bomb.left = snail.left + snail.bomb.width/2;
         snail.bomb.visible = false;
  
         // Snail bombs maintain a reference to their snail

         snail.bomb.snail = snail;

         this.sprites.push(snail.bomb);
      }      
   },

   isSpriteInView: function(sprite) {
      return sprite.left + sprite.width > sprite.hOffset &&
             sprite.left < sprite.hOffset + this.canvas.width;
   },

   updateSprites: function (now) {
      var sprite;

      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if (sprite.visible && this.isSpriteInView(sprite)) {
            sprite.update(now, 
             this.fps, 
             this.context,
             this.lastAnimationFrameTime);
         }
      }
   },

   drawSprites: function() {
      var sprite;

      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if (sprite.visible && this.isSpriteInView(sprite)) {
            this.context.translate(-sprite.hOffset, 0);
            sprite.draw(this.context);
            this.context.translate(sprite.hOffset, 0);
         }
      }
   },

    //Animation
    animate: function(now){
       //Replace the time that is worked out by the browser
       //with our own gametime
       now = snailBait.timeSystem.calculateGameTime();
        if(snailBait.paused){
            setTimeout(function () {
                requestAnimationFrame(snailBait.animate);
            }, snailBait.PAUSE_CHECK_INTERVAL);
        }
        else{
            snailBait.fps = snailBait.calculateFps(now);
            snailBait.draw(now);
            snailBait.lastAnimationFrameTime = now;
            requestAnimationFrame(snailBait.animate);
        }
    },

    setTimeRate: function(rate){
       
       this.timeRate = rate;
       this.timeSystem.setTransducer(function(now){
          return now * snailBait.timeRate;
       });
    },

    calculateFps: function(now){
        var fps = 1/(now - this.lastAnimationFrameTime) * 1000 * snailBait.timeRate;
        if(now - this.lastFpsUpdateTime > 1000){
            this.lastFpsUpdateTime = now;
            this.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
        }
        return fps;
    },

    backgroundLoaded: function () {
        var LOADING_SCREEN_TRANSITION_DURATION = 2000;
  
        this.fadeOutElements(this.loadingElement, 
                             LOADING_SCREEN_TRANSITION_DURATION);
  
        setTimeout ( function () {
           snailBait.startGame();
           snailBait.gameStarted = true;
        }, LOADING_SCREEN_TRANSITION_DURATION);
     },
  
     loadingAnimationLoaded: function () {
        if (!this.gameStarted) {
           this.fadeInElements(this.snailAnimatedGIFElement,
                               this.loadingTitleElement);
        }
     },

    initializeImages: function (){
        this.spritesheet.src = 'images/spritesheet.png';
        this.snailAnimatedGIFElement.src = 'images/snail.gif';

        this.spritesheet.onload = function (e) {
           snailBait.backgroundLoaded();
        };
  
        this.snailAnimatedGIFElement.onload = function () {
           snailBait.loadingAnimationLoaded();
        };
    },

    dimControls: function () {
        FINAL_OPACITY = 0.5;
  
        snailBait.instructionsElement.style.opacity = FINAL_OPACITY;
        snailBait.soundAndMusicElement.style.opacity = FINAL_OPACITY;
     },
  
     revealCanvas: function () {
        this.fadeInElements(this.canvas);
     },
  
     revealTopChrome: function () {
        this.fadeInElements(this.fpsElement,
                            this.scoreElement);
     },
  
     revealTopChromeDimmed: function () {
        var DIM = 0.25;
  
        this.scoreElement.style.display = 'block';
        this.fpsElement.style.display = 'block';
  
        setTimeout( function () {
           snailBait.scoreElement.style.opacity = DIM;
           snailBait.fpsElement.style.opacity = DIM;
        }, this.SHORT_DELAY);
     },
  
     revealBottomChrome: function () {
        this.fadeInElements(this.soundAndMusicElement,
                            this.instructionsElement,
                            this.copyrightElement);
     },
  
     revealGame: function () {
        var DIM_CONTROLS_DELAY = 5000;
  
        this.revealTopChromeDimmed();
        this.revealCanvas();
        this.revealBottomChrome();
  
        setTimeout( function () {
           snailBait.dimControls();
           snailBait.revealTopChrome();
        }, DIM_CONTROLS_DELAY);
     },   
  
     revealInitialToast: function () {
        var INITIAL_TOAST_DELAY = 1500,
            INITIAL_TOAST_DURATION = 3000;
  
        setTimeout( function () {
           snailBait.revealToast('Collide with coins and jewels. ' +
                                 'Avoid bats and bees.', 
                                 INITIAL_TOAST_DURATION);
        }, INITIAL_TOAST_DELAY);
     },

     explode: function(sprite){
        if(!sprite.exploding)
        {
           if(sprite.runAnimationRate === 0){
              sprite.runAnimationRate = snailBait.RUN_ANIMATION_RATE;
           }
           sprite.exploding = true;
        } 
     },

     shake: function () {
      var SHAKE_INTERVAL = 80, // milliseconds
          v = snailBait.BACKGROUND_VELOCITY*1.5,
          ov = snailBait.bgVelocity; // ov means original velocity
   
      this.bgVelocity = -v;

      setTimeout( function (e) {
       snailBait.bgVelocity = v;
       setTimeout( function (e) {
          snailBait.bgVelocity = -v;
          setTimeout( function (e) {
             snailBait.bgVelocity = v;
             setTimeout( function (e) {
                snailBait.bgVelocity = -v;
                setTimeout( function (e) {
                   snailBait.bgVelocity = v;
                   setTimeout( function (e) {
                      snailBait.bgVelocity = -v;
                      setTimeout( function (e) {
                         snailBait.bgVelocity = v;
                         setTimeout( function (e) {
                            snailBait.bgVelocity = -v;
                            setTimeout( function (e) {
                               snailBait.bgVelocity = v;
                               setTimeout( function (e) {
                                  snailBait.bgVelocity = -v;
                                  setTimeout( function (e) {
                                     snailBait.bgVelocity = v;
                                     setTimeout( function (e) {
                                        snailBait.bgVelocity = ov;
                                     }, SHAKE_INTERVAL);
                                  }, SHAKE_INTERVAL);
                               }, SHAKE_INTERVAL);
                            }, SHAKE_INTERVAL);
                         }, SHAKE_INTERVAL);
                      }, SHAKE_INTERVAL);
                   }, SHAKE_INTERVAL);
                }, SHAKE_INTERVAL);
             }, SHAKE_INTERVAL);
          }, SHAKE_INTERVAL);
       }, SHAKE_INTERVAL);
     }, SHAKE_INTERVAL);
   },

    startGame: function (){
        
        this.revealGame();
        this.revealInitialToast();
        this.playing = true;
        this.timeSystem.start();
        snailBait.setTimeRate(1.0);
        requestAnimationFrame(this.animate);
    },

    draw: function(now){
        this.setPlatformVelocity();
        this.setOffsets(now);
        this.drawBackground();
        this.updateSprites(now);
        this.drawSprites();
    },

    setPlatformVelocity: function(){
        this.platformVelocity = this.bgVelocity * 
        this.PLATFORM_VELOCITY_MULTIPIER;
    },

    setOffsets: function(now){
        this.setBackgroundOffset(now);
        this.setSpriteOffsets(now);
    },

    setBackgroundOffset: function(now){
        this.backgroundOffset += this.bgVelocity * 
        (now - this.lastAnimationFrameTime)/1000;
        if(this.backgroundOffset < 0 || this.backgroundOffset > 
            this.BACKGROUND_WIDTH){
                this.backgroundOffset = 0;
            }
    },

    setSpriteOffsets: function (now) {
      var sprite;
   
      // In step with platforms
      this.spriteOffset +=
         this.platformVelocity * (now - this.lastAnimationFrameTime) / 1000;

      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if ('runner' !== sprite.type) {
            sprite.hOffset = this.spriteOffset; 
         }
      }
   },

   calculatePlatformTop: function (track) {
      if      (track === 1) { return this.TRACK_1_BASELINE; } // 323 pixels
      else if (track === 2) { return this.TRACK_2_BASELINE; } // 223 pixels
      else if (track === 3) { return this.TRACK_3_BASELINE; } // 123 pixels
   },

   putSpriteOnTrack: function(sprite, track){
      sprite.track = track;
      sprite.top = this.calculatePlatformTop(sprite.track) 
      - sprite.height; 
   },

   platformUnderneath: function(sprite, track){
      var platform, platformUnderneath,
      spriteCollisionRectangle = 
      sprite.calculateCollisionRectangle(),
      platformCollisionRectangle;

      if(track == undefined){
         track = sprite.track;
      }

      for(var i=0; i < snailBait.platforms.length; ++i){
         platform = snailBait.platforms[i];
         platformCollisionRectangle = platform.calculateCollisionRectangle();

         if(track === platform.track){
            if(spriteCollisionRectangle.right > 
               platformCollisionRectangle.left && 
               spriteCollisionRectangle.left < platformCollisionRectangle.right){
                  platformUnderneath = platform;
                  break;
               }
         }
      }
      return platformUnderneath;
   },

    turnLeft: function(){
        this.bgVelocity = -this.BACKGROUND_VELOCITY;
        this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
        this.runner.artist.cells = this.runnerCellsLeft;
    },

    turnRight: function(){
        this.bgVelocity = this.BACKGROUND_VELOCITY;
        this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
        this.runner.artist.cells = this.runnerCellsRight;
    },



    drawBackground: function () {
      var BACKGROUND_TOP_IN_SPRITESHEET = 590;

      // Translate everything by the background offset
      this.context.translate(-this.backgroundOffset, 0);

      // 2/3 onscreen initially:
      this.context.drawImage(
         this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET, 
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT,
         0, 0,
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT);

      // Initially offscreen:
      this.context.drawImage(
         this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET, 
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT,
         this.BACKGROUND_WIDTH, 0,
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT);

      // Translate back to the original location
      this.context.translate(this.backgroundOffset, 0);
   },

    drawPlatforms: function(){
        var index;
        this.context.translate(-this.platformOffset, 0);
        for(index =0; index < this.platformData.length; ++index){
            this.drawPlatform(this.platformData[index]);
        }
        this.context.translate(this.platformOffset, 0);
    },

    drawPlatform: function(data){
        var platformTop = this.calculatePlatformTop(data.track);

        this.context.lineWidth = this.PLATFORM_STOKE_WIDTH;
        this.context.strokeStyle = this.PLATFORM_STROKE_STYLE;
        this.context.fillStyle = data.fillStyle;
        this.context.globalAlpha = data.opacity;

        this.context.strokeRect(data.left, platformTop, 
            data.width, this.PLATFORM_HEIGHT);
            this.context.fillRect(data.left, platformTop, data.width, 
            data.height);
    },

    putSpriteOnPlatform: function(sprite, platformSprite) {
      sprite.top  = platformSprite.top - sprite.height;
      sprite.left = platformSprite.left;
      sprite.platform = platformSprite; 
   },

    calculatePlatformTop: function(track){
        if(track === 1){
            return this.TRACK_1_BASELINE;
        }
        else if(track ===2){
            return this.TRACK_2_BASELINE;
        }
        else if(track === 3){
            return this.TRACK_3_BASELINE;
        }
    },

    togglePausedStateOfAllBehaviors: function (now) {
      var behavior;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         for (var j=0; j < sprite.behaviors.length; ++j) {
            behavior = sprite.behaviors[j];

            if (this.paused) {
               if (behavior.pause) {
                  behavior.pause(sprite, now);
               }
            }
            else {
               if (behavior.unpause) {
                  behavior.unpause(sprite, now);
               }
            }
         }
      }
   },

    togglePaused: function(){
        var now = this.timeSystem.calculateGameTime();
        this.paused = !this.paused;

        this.togglePausedStateOfAllBehaviors(now);

        if(this.paused){
            this.pauseStartTime = now;
        }
        else{
            this.lastAnimationFrameTime += 
            (now - this.pauseStartTime);
        }
    },

    fadeInElements: function () {
        var args = arguments;
  
        for (var i=0; i < args.length; ++i) {
           args[i].style.display = 'block';
        }
  
        setTimeout( function () {
           for (var i=0; i < args.length; ++i) {
              args[i].style.opacity = snailBait.OPAQUE;
           }
        }, this.SHORT_DELAY);
     },
  
     fadeOutElements: function () {
        var args = arguments,
            fadeDuration = args[args.length-1]; // Last argument
  
        for (var i=0; i < args.length-1; ++i) {
           args[i].style.opacity = this.TRANSPARENT;
        }
  
        setTimeout(function() {
           for (var i=0; i < args.length-1; ++i) {
              args[i].style.display = 'none';
           }
        }, fadeDuration);
     },
  
     hideToast: function () {
        var TOAST_TRANSITION_DURATION = 450;
  
        this.fadeOutElements(this.toastElement, 
                             TOAST_TRANSITION_DURATION); 
     },
  
     startToastTransition: function (text, duration) {
        this.toastElement.innerHTML = text;
        this.fadeInElements(this.toastElement);
     },

    revealToast: function (text, duration) {
        var DEFAULT_TOAST_DISPLAY_DURATION = 1000;
        
        duration = duration || DEFAULT_TOAST_DISPLAY_DURATION;
  
        this.startToastTransition(text, duration);

        setTimeout( function (e) {
            snailBait.hideToast();
        }, duration);
     },

     loseLife: function(){
        var TRANSITION_DURATION = 3000;

        this.lives--;
        this.startLifeTransition(snailBait.RUNNER_EXPLOSION_DURATION);
        
        setTimeout( function(){
           snailBait.endLifeTransition();
        }, TRANSITION_DURATION);
     },

     startLifeTransition: function(slowMotionDelay){
        var CANVAS_TRANSITION_OPACITY = 0.05,
        SLOW_MOTION_RATE = 0.1;

        this.canvas.style.opacity = CANVAS_TRANSITION_OPACITY;
        this.playing = false;
      
        setTimeout(function(){
           snailBait.setTimeRate(SLOW_MOTION_RATE);
           snailBait.runner.visible = false;
        }, slowMotionDelay);
     },

     endLifeTransition: function(){
        var TIME_RESET_DELAY = 1000,
        RUN_DELAY = 500;

        snailBait.reset();

        setTimeout(function(){
           snailBait.setTimeRate(1.0);

           setTimeout(function(){
              snailBait.runner.runAnimationRate = 0;
              snailBait.playing = true;
           }, RUN_DELAY);
        }, TIME_RESET_DELAY);
     },

     reset: function(){
        snailBait.resetOffsets();
        snailBait.resetRunner();
        snailBait.makeAllSpritesVisible();
        snailBait.canvas.style.opacity = 1.0;
     },

     resetOffsets: function(){
        this.bgVelocity = 0;
        this.backgroundOffset = 0;
        this.platformOffset = 0;
        this.spriteOffset = 0;
     },

     resetRunner: function(){
        var RUNNER_RESET_PLATFORM = 3;
        this.runner.left = snailBait.RUNNER_LEFT;
        this.runner.track = RUNNER_RESET_PLATFORM;
        this.runner.hOffset = 0;
        this.runner.visible = true;
        this.runner.exploding = false;
        this.runner.falling = false;
        this.runner.jumping = false;
        this.runner.top = this.calculatePlatformTop(RUNNER_RESET_PLATFORM) - 
        this.runner.height;
        this.runner.artist.cells = this.runnerCellsRight;
        this.runner.artist.cellIndex = 0;
     },

     makeAllSpritesVisible: function(){
        for(var i=0; i < snailBait.sprites.length; ++i){
           snailBait.sprites[i].visible = true;
        }
     }
}

//Event handlers
window.addEventListener('keydown', function(e){
    var key = e.keyCode;

    if(!snailBait.playing || snailBait.runner.exploding){
       return;
    }

   if(key === snailBait.A_KEY || key === snailBait.LEFT_ARROW){
      snailBait.turnLeft();
   }
   else if(key === snailBait.D_KEY || key === snailBait.RIGHT_ARROW){
      snailBait.turnRight();
   }
   else if(key === snailBait.P_KEY){
      snailBait.togglePaused();
   }
   else if(key === snailBait.J_KEY){
      snailBait.runner.jump();
   } 
});

window.onblur = function (e) {  // pause if unpaused
    snailBait.windowHasFocus = false;
    
    if ( ! snailBait.paused) {
       snailBait.togglePaused();
    }
 };
 
 window.onfocus = function (e) {  // unpause if paused
    var originalFont = snailBait.toastElement.style.fontSize,
        DIGIT_DISPLAY_DURATION = 1000; // milliseconds
 
    snailBait.windowHasFocus = true;
    snailBait.countdownInProgress = true;
 
    if (snailBait.paused) {
       snailBait.toastElement.style.font = '128px fantasy'; // Large font
 
       if (snailBait.windowHasFocus && snailBait.countdownInProgress)
          snailBait.revealToast('3', 500); // Display 3 for 0.5 seconds
 
       setTimeout(function (e) {
          if (snailBait.windowHasFocus && snailBait.countdownInProgress)
             snailBait.revealToast('2', 500); // Display 2 for 0.5 seconds
 
          setTimeout(function (e) {
             if (snailBait.windowHasFocus && snailBait.countdownInProgress)
                snailBait.revealToast('1', 500); // Display 1 for 0.5 seconds
 
             setTimeout(function (e) {
                if ( snailBait.windowHasFocus && snailBait.countdownInProgress)
                   snailBait.togglePaused();
 
                if ( snailBait.windowHasFocus && snailBait.countdownInProgress)
                   snailBait.toastElement.style.fontSize = originalFont;
                   
                snailBait.countdownInProgress = false;
 
             }, DIGIT_DISPLAY_DURATION);
 
          }, DIGIT_DISPLAY_DURATION);
 
       }, DIGIT_DISPLAY_DURATION);
    }
 };

 //Launch Game
var snailBait = new SnailBait();
snailBait.initializeImages();
snailBait.createSprites();







